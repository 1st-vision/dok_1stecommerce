.. |label| replace:: Office Line Zusatzmodul (Add-In) 1steCommerce
.. |technicalname| replace:: 1steCommerce
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 7.1
.. |maxVersion| replace:: 9.0.6
.. |version| replace:: 9

|label|
===============================================

Überblick
---------
:Author: |Author|
:Kürzel: |technicalname|
:kompatibel für Sage 100: |minVersion| bis |maxVersion|
:Version: |version|

.. tip::

   Dieses Modul wird aktuell weiter entwickelt.
    
.. toctree::
   :caption: Modul-Beschreibung

   Überblick <self>
   
Beschreibung
============
Das |label| wird in der Standard-Version mit dem OLSI mitgeliefert und kann optional installiert und eingesetzt werden. 
Der Einsatz mit einer Office Line Basic Version erfordert die Sage Office Line Add-In Options-Lizenz, welche Sie über Ihren Sage-Fachhändler erwerben können. 
Bei der Office Line Business ist das Modul frei einsetzbar. Damit können Sie Shop-Kategorien mit unbegrenzter Baumtiefe unabhängig von den Office Line Artikelgruppen pflegen, 
mit erweiterten mehrsprachigen Texten für Kategorien und der Möglichkeit Artikel zu einer oder mehreren Kategorien zuzuordnen. 
   
.. toctree::
   :caption: Installation und Konfiguration

   install
   config
   
.. toctree::
   :caption: Verwendung
   
   verwendung