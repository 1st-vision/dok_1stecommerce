Konfiguration
=============

Unter dem Regiezentrum-Eintrag 1st Vision eCommerce Solutions/ Shop-Konfiguration muss der Shop definiert und konfiguriert werden. 
Sie können dann die Shop-Konfiguration vornehmen.

Grundlagen
----------

.. image:: images/eCommerce4.png

:Shop-ID: Um einen Shop zu eröffnen tragen Sie einfach eine Shop-Nummer beginnend mit 1 in das Feld Shop-ID ein.
:Shop-Bezeichnung: Die Shop-Bezeichnung ist optional und dient der Identifizierung.
:Artikelbilder-Ordner: Der Artikel-Bilder-Ordner sollte ein freigegebener Ordner auf dem Server oder PC sein, auf dem auch das LSI läuft, dorthin werden die in der Bilderverwaltung ausgewählten Bilder kopiert, so dass das OLSI diese dann per FTP mit zum Webserver übertragen kann.
:Artikel-Bilder-Kategorien: Die Artikel-Bilder-Kategorien können verwendet werden um für die Artikelbilder im Dateinamen noch Kategorie-Angaben, sowie Variantenangaben (werden bei Varianten-Artikel dann automatisch angefügt) dazu zufügen, wenn das Artikelbild umkopiert und benannt wird. Die gewünschten Kategorien sind Semikolon separiert anzugeben, z.B.: "Thumb;Big". Beim Kopieren des Artikelbildes in den konfigurierten Bilder-Ordner kann nun durch Vorauswahl der Kategorie (siehe 4.3 Register Bilder) der erzeugte Bild -Dateiname die gewählte Kategorie enthalten, z.B.: 16800010_Thumb_1.jpg
:Artikel in Sammelmappe: mit dieser Option können Sie die Bilder die im definierten Ordner liegen in die Sammelmappe der OfficeLine importieren. 
:HTML-Artikeltext CSS: Hier können Sie eine CSS-Datei einbinden die von dem eingebauten HTML-Editor in unserem Modul interpretiert wird.
:HTML-Artikeltext Styles: Hier können Sie eine XML-Datei einbinden die von dem eingebauten HTML-Editor in unserem Modul interpretiert wird.
:Trennzeichen nach Artikel-Nr: Im Standard wird das Trennzeichen "_" benutzt um Artikelnummer und weitere Merkmale sauber zu trennen. Sollten Sie aber "_" in der Artikelnummer benutzen empfiehlt es sich auf ein anderes Trennzeichen zu wechseln.
:Kategoriebilder-Ordner: Der Kategorie-Bilder-Ordner sollte ein freigegebener Ordner auf dem Server oder PC sein, auf dem auch das OLSI läuft, dorthin werden die in der Bilderverwaltung ausgewählten Bilder kopiert, so dass das OLSI diese dann per FTP mit zum Webserver übertragen kann.
:Kategorie-Templates: Hier können Sie ein Dropdown definieren für die Kategorien. (Symikolen-getrennt)
:Kategoriebaum dynamisch: Wenn Sie zuviele Kategorien haben, kann man dies auf "JA" setzen, dann wird die kategoriestruktur erst beim klicke geladen.
:Artikelsuche-Spalten 4-x: Hier können Sie Spalten der Tabelle KHKArtikel beim hinzufügen von Artikeln zusätzlich mit einblenden lassen. (max. 4 zusätzliche Spalten)
:OLSI-Server-Port: http-adresse des OLSI Servers mit CommandPort.


Medien
------

.. image:: images/eCommerce5.png

:Datenblatt-Ordner: Hier können Sie den Odner für die Datenblätter hinterlegen.
:Datenblatt in Sammelmappe-Nr.: Wenn Sie die Datenblätter automatisiert an die Sammelmappe übergeben möchten, müssen Sie hier die Mappe der Sammelmappe hinterlegen.
:Daten-/Medien-Ordner 2: Hier können Sie einen Odner für weitere Datenblätter/Medien hinterlegen.
:Eintrag2 in Sammelmappe Nr.: Wenn Sie die Datenblätter/Medien automatisiert an die Sammelmappe übergeben möchten, müssen Sie hier die Mappe der Sammelmappe hinterlegen.
:Daten-/Medien-Ordner 3: Hier können Sie einen Odner für weitere Datenblätter/Medien hinterlegen.
:Eintrag3 in Sammelmappe Nr.: Wenn Sie die Datenblätter/Medien automatisiert an die Sammelmappe übergeben möchten, müssen Sie hier die Mappe der Sammelmappe hinterlegen.
:Daten-/Medien-Ordner 4: Hier können Sie einen Odner für weitere Datenblätter/Medien hinterlegen.
:Eintrag4 in Sammelmappe Nr.: Wenn Sie die Datenblätter/Medien automatisiert an die Sammelmappe übergeben möchten, müssen Sie hier die Mappe der Sammelmappe hinterlegen.

Artikelupdate
-------------

.. image:: images/eCommerce6.png

:Artikel-Update URL: Der Artikel-Update Link bietet die Option aus dem Artikelstamm heraus den hier konfigurierten Link mit den entsprechenden Werten aus dem jeweiligen Artikel direkt aufzurufen (Optionen – 1steCommerce:Artikel-Update), damit kann der Lagerbestand und der Preis eines Artikels sofort im Shop aktualisiert werden, sofern der Shop auf der Gegenseite den Empfang eines solchen Links auch unterstützt und diesen entsprechend verarbeiten kann. Der jeweils zuletzt aufgerufene Link wird hier als Referenz bzw. zur Überprüfung unter letzter Aufruf (Artikel-Update) eingetragen.
:Benutzername/Login und Passwort: Benutzername/Login und Passwort kann beim Artikel-Update-Link jeweils mit übergeben werden falls nötig.
:Preislisten-Filter: Beim Preislisten-Filter können eine oder mehrere Preislisten (Kommagetrennt) hinterlegt werden, welche dann beim Update-Link mit übergeben werden, alle anderen Preislisten werden nicht mit übergeben. (z.B.: Standard, Endkunde)