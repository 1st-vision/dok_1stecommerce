Verwendung
==========

Pflege der Shop-Artikeltexte
----------------------------

Die Pflege der Webshop-Artikeltexte kann aus dem Artikelstamm heraus über
Optionen – 1steCommerce:Webshop-Artikeltexte aufgerufen werden.

Grundlagen
__________
Sie können hier in den Registern Grundlagen die Webshop-Artikeltexte und Zusatzdaten pflegen (sprachabhängig).

.. image:: images/eCommerce7.png

:Sprache auswählen: Hier können Sie die Sprachen die in der OfficeLine hinterlegt sind auswählen und je nachdem welche Sprache Sie pflegen möchten diese auswählen.
:Shop-ID: Hier kann der Shop/Konfiguration die hinterlegt wurde auswählen. Nicht alle Felder sind pro Shop-ID unterschiedlich.
:OL-Dimensionstext: Es wird der hinterlegt Dimensionstext für diesen Artikel angezeigt und kann kopiert werden und in ein anderes Feld eingefügt werden.
:OL-Langtext: Es wird der hinterlegt Langtext für diesen Artikel angezeigt und kann kopiert werden und in ein anderes Feld eingefügt werden.
:Varianten: Bei Varianten-Artikeln (Varianten-Modul der Office Line Business) kann hier jede einzelne Variante für den Webshop auf Aktiv/nicht Aktiv eingestellt werden. Beachten Sie hierzu aber auch die Konfiguration des Artikel-Exports, hier müssen die Filter bzw. SQL-WHERE-Klauseln auch entsprechend definiert und konfiguriert werden.
:Produkt-Link: Hier kann ein Produktlink für diesen Artikel hinterlegt werden.
:Suchbegriffe: Hier können die Suchbegriffe für diesen Artikel hinterlegt werden.
:META-Titel/META-Description/META-Keywords: Im Standard werden diese Felder als METAs übergeben und können hier gepflegt werden.
:abw. Bezeichnung1: Wenn die Bezeichnung des Artikels anders sein soll als der der hinterlegt ist, kann dies hier eingetragen werden.

12Trade / Ebay / Optionale Zusatzfelder
***************************************

optionale Felder
Diese Felden sind für die EDI-Schnittstelle zu Ebay vorgesehen, können aber optional für einen Webshop benutzt werden.

:Versandklasse: dies ist ein optionales Feld
:Ebay-Bezeichnung: dies ist ein optionales Feld
:Ebay-Untertitel: dies ist ein optionales Feld
:Ebay-Kategorie-Nr: dies ist ein optionales Feld
:Ebay-Shop-Kat.-Nr: dies ist ein optionales Feld
:GalerieBild-URL: dies ist ein optionales Feld

HTML-/Lang-Texte
________________
Sie können hier in den Registern Langtexte/HTML-Texte die Webshop-Artikeltexte und Zusatzdaten pflegen (sprachabhängig).

.. image:: images/eCommerce8.png

:Sprache auswählen: Hier können Sie die Sprachen die in der OfficeLine hinterlegt sind auswählen und je nachdem welche Sprache Sie pflegen möchten diese auswählen.
:Shop-ID: Hier kann der Shop/Konfiguration die hinterlegt wurde auswählen. Nicht alle Felder sind pro Shop-ID unterschiedlich.
:Webshop-Artikeltext im Quelltextformat: Hier wird der Artikeltext hinterlegt im Quelltextformat.
:Webshop-Artikeltext mit HTML-Editor bearbeiten: Hier wird der Artikeltext hinterlegt.
:Webshop-Kurzbezeichnung (EBAY) im Quelltextformat: Falls benötigt kann hier ein extra Artikeltext gepflegt werden im Quelltextformat.
:Webshop-Kurzbezeichnung (EBAY) mit HTML-Editor bearbeiten: Falls benötigt kann hier ein extra Artikeltext gepflegt werden.

Bilder
______
Im Register Bilder können Sie die Artikel-Bilder aus dem konfigurierten Bilder-Ordner
verwalten. Hier werden alle im Bilder-Ordner befindlichen Dateien mit den Dateinamen
Artikelnummer*.* gelesen und angezeigt als Bilder zum gewählten Artikel.

.. image:: images/eCommerce9.png

:Sprache auswählen: Hier können Sie die Sprachen die in der OfficeLine hinterlegt sind auswählen und je nachdem welche Sprache Sie pflegen möchten diese auswählen.
:Shop-ID: Hier kann der Shop/Konfiguration die hinterlegt wurde auswählen. Nicht alle Felder sind pro Shop-ID unterschiedlich.

Die Bilder können hier durchgeblättert werden, durch den Löschen-Button wird die
Bilddatei aus dem Server-Ordner gelöscht. Mit dem Neu-Button kann ein neues Bild
ausgewählt werden, dieses wird dann in den Artikel-Bilder-Ordner umkopiert und
gleichzeitig umbenannt in Artikelnummer_BildKategorie_LfdNr, bzw. Artikelnummer_LfdNr wenn die Bild-Kategorie leer ist.
Durch klicken auf das Artikelbild kann zwischen Zoom-Ansicht und Originalgrößen-
Darstellung gewechselt werden.

:Bild-Kategorie für neues Bild: Die Artikel-Bilder-Kategorien können verwendet werden um für die Artikelbilder im Dateinamen noch Kategorie-Angaben, sowie Variantenangaben (werden bei Varianten-Artikel dann automatisch angefügt) dazu zufügen, wenn das Artikelbild umkopiert und benannt wird.

Datenblätter
_____________
Im Register Datenblätter können Sie Medien(PDF) aus dem konfigurierten Ordner
verwalten. Hier werden alle im Ordner befindlichen Dateien mit den Dateinamen
Artikelnummer*.pdf gelesen und angezeigt zum gewählten Artikel.

.. image:: images/eCommerce10.png

:Sprache auswählen: Hier können Sie die Sprachen die in der OfficeLine hinterlegt sind auswählen und je nachdem welche Sprache Sie pflegen möchten diese auswählen.
:Shop-ID: Hier kann der Shop/Konfiguration die hinterlegt wurde auswählen. Nicht alle Felder sind pro Shop-ID unterschiedlich.

Die PDFs können hier durchgeblättert werden, durch den Löschen-Button wird die
Datei aus dem Server-Ordner gelöscht. Mit dem Neu-Button kann eine neue PDF-Datei
ausgewählt werden, dieses wird dann in den Medien-Ordner umkopiert und
gleichzeitig umbenannt in Artikelnummer_Sprache_Bezeichnung.

Weitere Felder
________________
Im Register Weitere Felder werden die USER_Felder von der Tabelle 1stECShopArtikelBezeichnung angezeigt und können hier editiert werden.

.. image:: images/eCommerce11.png

:Sprache auswählen: Hier können Sie die Sprachen die in der OfficeLine hinterlegt sind auswählen und je nachdem welche Sprache Sie pflegen möchten diese auswählen.
:Shop-ID: Hier kann der Shop/Konfiguration die hinterlegt wurde auswählen. Nicht alle Felder sind pro Shop-ID unterschiedlich.

Kalkulation
___________
Im Register Kalkulation wird der Artikel und die dazugehörigen Varianten aufgelistet.
Eine Übersicht mit LEK und KEK zu den VK-Preis mit Deckungsbetrag wird errechnet.

.. image:: images/eCommerce12.png

:Sprache auswählen: Hier können Sie die Sprachen die in der OfficeLine hinterlegt sind auswählen und je nachdem welche Sprache Sie pflegen möchten diese auswählen.
:Shop-ID: Hier kann der Shop/Konfiguration die hinterlegt wurde auswählen. Nicht alle Felder sind pro Shop-ID unterschiedlich.

Kategorien-Modul
----------------
Mit dem Kategorien-Modul können Sie die Webshop-
Kategorien unabhängig von den Office Line Artikelgruppen und in unbegrenzter Baumtiefe
definieren.
Sie können Artikel mehreren Kategorien und auch direkt der TOP Kategorie zuordnen.
Vor Einsatz des Kategorien-Moduls ist zu klären, ob das eingesetzte Shop-System auch
den Import der hier definierten Kategorien und Zuordnungen unterstützt.

Kategorien können Sie auf der linken Seite in der Baum-Darstellung mit der Funktion der
rechten Maustaste anlegen und löschen.

Grundlagen
__________
Im Register Grundlagen können Sie die weiteren Daten wie Sortierung etc. und die
sprachabhängigen Bezeichnungstexte hinterlegen. Der Kategorie-Matchcode dient nur
der Pflege und Anzeige hier in der Office Line, im Shop werden die unter der Sprache
eingetragenen Bezeichnungstexte verwendet.
Durch Konfiguration von mehreren Shops können Sie für jede Shop-ID eine eigene
Kategorien-Struktur und Artikelzuordnung definieren, hierfür ist es nötig, dass im
Artikelstamm für jede Shop-ID zur Kategorien-Zuordnung ein eigenes benutzerdefiniertes
Feld angelegt und bei der Shop-Konfiguration (siehe Konfiguration / Einstellungen) hinterlegt ist.
ACHTUNG:
Bitte die Einträge und Änderungen der Kategorien immer mit dem Speichern-Button
abschließen und speichern.

.. image:: images/eCommerce13.png

:Sortierung Kategorien: Mit Hilfe diesen Feldes können Sie die Sortierung der Kategorie innerhalb der Hierarchie steuern.
:Status: Sie können jederzeit die Kategorie aktivieren und deaktivieren, wenn die Kategorie in aktiv ist wird sie im Shop ebenso auf inaktiv gesetzt.
:Sortierung Artikel in Gruppe: Diese Funktion wird bei den modernen Shops nicht mehr unterstützt, da der Besucher selbst wählen kann wie er die Sortierung angezeigt haben möchte.
:Sortierungsart: Diese Funktion wird bei den modernen Shops nicht mehr unterstützt, da der Besucher selbst wählen kann wie er die Sortierung angezeigt haben möchte.
:Template: Ein optionales Feld für die Kategorie.
:Option / Extra-Angaben: Ein optionales Feld für die Kategorie.
:OL Artikelgruppen-Sync: Die können hier eine spezielle Artikelgruppe mit der ausgewählten Kategorie verbinden und jederzeit fehlende Artikel auf Knopfdruck hinzufügen.
:Löschen: Wenn dieses Feld auf JA steht wird die Synchronisation 1:1 von Artikelgruppe zu Kategorie ausgeführt,  die Artikel die in der Kategorie zugeordnet sind aber nicht in der Artikelgruppe mehr sind werden wieder rausgenommen. Bei Nein wird immer nur von der Artikelgruppe die fehlenden Artikel hinzugefügt.
:Sprache auswählen: Die Kategorie kann für mehrere Sprachen gesetzt werden. Alle Sprachen die in der OfficeLine hinterlegt sind, stehen zur Auswahl.
:Bezeichnung: Dies ist der Titel der Kategorie.
:Überschrift: Dies ist der Headliner für die Kategorie wenn der Shop dies unterstützt.
:META-Titel/META-Description/META-Keywords: Im Standard werden diese Felder als METAs übergeben und können hier gepflegt werden.
:Beschreibung(Text/HTML): Hier können Sie einen Beschreibungstext für die Kategorie hinterlegen und je nach Wunsch von Text auf HTML wechseln.

:Artikel zu Kategorien synchronisieren: Wenn Sie diesen Button drücken wird die Synchronisation gestartet, wenn Sie wie oben die Felder für die OL Artikelgruppen-Sync entsprechend gepflegt haben.

Artikel
_______
Im Reiter Artikel können Sie über die rechte Maustaste die Artikel den Kategorien hinzugefügt.

.. image:: images/eCommerce14.png

.. image:: images/eCommerce18.png

Hierzu haben Sie mehrere Möglichkeiten:

Webshop-Artikeltexte(rechtsklick auf einen bereits zugeordneten Artikel)
************************************************************************
Mit der Option Webshop-Artikeltexte oder einem Doppelklick können direkt die Webshop-Artikeltexte zu dem gewählten Artikel bearbeitet werden (siehe Pflege der Shop-Artikeltexte).
:Artikel bearbeiten: (rechtsklick auf einen bereits zugeordneten Artikel)

Artikel hinzufügen
******************
Mit der Option Artikel hinzufügen können aus der eingeblendeten Artikel-Liste per Doppelklick die Artikel der Kategorie hinzugefügt werden. Hier kann die Selektion und Auswahl der Artikel auch über die übliche Listen-Filter Funktion gemacht werden.

.. image:: images/eCommerce19.png

Artikelgruppe hinzufügen
************************
Mit der Option Artikelgruppe hinzufügen kann eine gesamte Artikelgruppe der OfficeLine der gewählten Kategorie zugeordnet werden.

Alle Artikel in der Kategorie entfernen
****************************************
Es werden alle Artikel aus der ausgewählten Kategorie entfernt.

Sortierung initialisieren
*************************
Die Sortierung wird fortlaufen gesetzt.

Artikelbild (rechtsklick auf einen bereits zugeordneten Artikel)
****************************************************************
Es wird das Artikelbild des Artikels angezeigt.

Cross-Selling/Sync erw. (rechtsklick auf einen bereits zugeordneten Artikel)
****************************************************************************
In dieser Maske können Sie Cross-Selling Artikel hinterlegen.

.. image:: images/eCommerce20.png

:Cross-Typ: 
 - Artikel-Link: Sie weisen einen speziellen Artikel zu
 - Kategorie-Link: Sie weisen alle Artikel einer Kategorie zu

:Cross-Art:
 - Cross-Selling: Dies wird als Ähnliche Artikel im Shop angezeigt
 - Up-Selling: kein Standard, kann aber jederzeit benutzt werden
 - Zubehör: Dies wird als Zubehör im Shop angezeigt

:Cross-Selling Artikel: Wenn Sie als Typ Artikel-Link ausgewählt haben, können Sie hier die Artikelnummer suchen oder eintragen.

:Cross-Selling Kategorie: Wenn Sie als Typ Kategorie-Link ausgewählt haben, können Sie hier die Kategorie suchen oder eintragen.

:Cross-Selling Link-Text: kein Standard, kann aber jederzeit benutzt werden

Artikel - Update Flag setzen/löschen (rechtsklick auf einen bereits zugeordneten Artikel)
*****************************************************************************************
Hier können Sie den gewählten Artikel markieren. Diese Auswirkung dieser Funktion muss speziell konfiguriert werden.

Alle Artikel in der Kategorie - Update Flag setzen
**************************************************
Hier können Sie alle Artikel in der ausgewählten Kategorie markieren. Diese Auswirkung dieser Funktion muss speziell konfiguriert werden.

Alle Artikel in der Kategorie - Update Flag löschen
***************************************************
Hier können Sie alle Artikel in der ausgewählten Kategorie demarkieren. Diese Auswirkung dieser Funktion muss speziell konfiguriert werden.

Bild
____
Im Register Bild können Sie die Kategorie-Bilder aus dem konfigurierten Bilder-Ordner
verwalten. Hier werden alle im Bilder-Ordner befindlichen Dateien mit den Dateinamen
Kategorie-ID*.* gelesen und angezeigt als Bilder zum gewählten Artikel.

.. image:: images/eCommerce15.png

Die Bilder können hier durchgeblättert werden, durch den Löschen-Button wird die
Bilddatei aus dem Server-Ordner gelöscht. Mit dem Neu-Button kann ein neues Bild
ausgewählt werden, dieses wird dann in den Kategorie-Bilder-Ordner umkopiert und
gleichzeitig umbenannt in Kategorie-ID_LfdNr.
Durch klicken auf das Kategoriebild kann zwischen Zoom-Ansicht und Originalgrößen-
Darstellung gewechselt werden.

Weitere Felder
______________
Im Register Weitere Felder können noch mehr spezielle Beschreibungen pro Sprache hinterlegt werden.
Dies muss bei der Übertragen entsprechend konfiguriert werden.

.. image:: images/eCommerce16.png

:Sprache: Hier wählen Sie die Sprache für die speziellen Info-Boxen der Kategorie aus.
:Info-Box Nr: Hier können Sie die speziellen Info-Boxen (von 1-9) auswählen.
:Info-Titel: Sie können der Info-Box einen Titel geben.
:Info-Link: Sie können der Info-Box eine weitere Eigenschaft/Link geben.
:Info-Text: Sie können der Info-Box eine Beschreibung geben. Hier können Sie wieder wechseln zwischen Text und HTML.

Sync erw.
_________
Im Register Sync erw.

.. image:: images/eCommerce17.png


OfficeLine Artikel-Maske
------------------------
Wenn Sie die neue Maske aktiviert haben müssen Sie im AppDesigner die Struktur "Vision_eCommerce.metadata" hinzufügen.
Sie finden diese Datei im OLSI Ordner.
 
.. code-block:: console
   
   C:\Program Files (x86)\Sage\Sage 100\9.0\OLSI

.. image:: images/eCommerce21.png

:Webshop-Artikeltexte: Hier springen Sie in die Maske für die Artikeltexte (siehe Pflege der Shop-Artikeltexte)
:Webshop-Kategorien: Hier öffnet sich der Kategoriebaum und Sie können durch markieren der Kategorie den Artikel direkt zuordnen.
:Webshop-Artikelupdate-Flag: Hier können Sie den Artikel mit dem update-Flag setzen.
:Webshop-Artikelbild: Hier wird Ihnen das Bild angezeigt, wenn Sie eins zugeordnet haben.