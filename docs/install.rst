Installation
============

Das 1steCommerce-AddIn ist ein Office Line Zusatzmodul (Add-In) für die Warenwirtschaft. Es kann zusammen mit dem Modul OLSI installiert werden, 
oder steht als eigener Setup zur Verfügung, so dass es auf jedem Warenwirtschafts-Client bei Bedarf installiert werden kann.

.. image:: images/eCommerce1.png

.. image:: images/eCommerce2.png

.. image:: images/eCommerce3.png